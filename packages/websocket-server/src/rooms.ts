import WebSocket from "ws";

// TODO: use redis or something for a proper datastore

const rooms: {
  [roomID: string]: Set<WebSocket>;
} = {};

export const getClientsFromRoomID = (roomID: string): WebSocket[] =>
  Array.from(rooms[roomID]);

export const addClientToRoom = (roomID: string, client: WebSocket): void => {
  if (!rooms[roomID]) {
    rooms[roomID] = new Set<WebSocket>();
  }
  if (!rooms[roomID].has(client)) {
    rooms[roomID].add(client);
  }
};

export const removeClient = (client: WebSocket): void => {
  Object.keys(rooms).forEach((roomID) => rooms[roomID].delete(client));
};
