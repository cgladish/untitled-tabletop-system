import WebSocket from "ws";
import { WEBSOCKET_PORT } from "./constants";
import { addClientToRoom, getClientsFromRoomID, removeClient } from "./rooms";

// TODO: ADD AUTH/HTTPS

const wss = new WebSocket.Server({ port: WEBSOCKET_PORT });

type Message = {
  roomID: string;
  timestamp: Date;
  type: "text"; // Extend this with other types (e.g. attack, etc)
  content: any;
};

wss.on("connection", (client) => {
  console.log("Client connected to server");

  client.on("message", (message: string) => {
    console.log("Received message from client\n" + message);

    let parsedMessage: Message;
    try {
      parsedMessage = JSON.parse(message) as Message;
    } catch (err) {
      console.error("Unable to parse message sent by client");
      throw err;
    }

    addClientToRoom(parsedMessage.roomID, client);
    const clientsInRoom = getClientsFromRoomID(parsedMessage.roomID);
    clientsInRoom.forEach((client) => client.send(message));

    console.log(
      `Successfully sent back message to ${clientsInRoom.length} clients`
    );
  });

  client.on("close", (code, reason) => {
    removeClient(client);

    console.log(
      "Client disconnected\n" + JSON.stringify({ code, reason }, null, 2)
    );
  });
});

console.log(`Websocket server listening at wss://localhost:${WEBSOCKET_PORT}`);
