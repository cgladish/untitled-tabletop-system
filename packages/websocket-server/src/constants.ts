if (process.env.NODE_ENV === "development") {
  require("dotenv").config({ path: "../../.env" });
}

export const WEBSOCKET_PORT = (process.env.WEBSOCKET_PORT &&
  Number(process.env.WEBSOCKET_PORT)) as number;

const requiredEnvVars = { WEBSOCKET_PORT };
const missingEnvVars = Object.entries(requiredEnvVars).filter(
  ([_, val]) => !val
);
if (missingEnvVars.length) {
  throw new Error(
    `Missing the following required env vars: ${missingEnvVars
      .map(([key, _]) => key)
      .join(", ")}`
  );
}
