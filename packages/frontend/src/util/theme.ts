import { useTheme as emotionUseTheme } from "emotion-theming";

type ThemeID = "dark" | "light";

export type Theme = {
  id: ThemeID;
  colors: {
    text: {
      primary: string;
      secondary: string;
      tertiary: string;
    };
    background: {
      primary: string;
      secondary: string;
      tertiary: string;
    };
    special: string;
  };
};

export const themes: Record<ThemeID, Theme> = {
  light: {
    id: "dark",
    colors: {
      text: {
        primary: "#000",
        secondary: "#444",
        tertiary: "#888",
      },
      background: {
        primary: "#fff",
        secondary: "#ddd",
        tertiary: "#bbb",
      },
      special: "#00A86B",
    },
  },
  dark: {
    id: "light",
    colors: {
      text: {
        primary: "#fff",
        secondary: "#bbb",
        tertiary: "#888",
      },
      background: {
        primary: "#444",
        secondary: "#666",
        tertiary: "#888",
      },
      special: "#00A86B",
    },
  },
};

export const useTheme = (): Theme => emotionUseTheme<Theme>();
