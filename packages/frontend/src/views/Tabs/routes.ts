import { mainRoutePath, MainRouteParams } from "../Main/routes";

export type TabID = "vault";
export const tabRoutePath = `${mainRoutePath}/:tabID`;
export type TabRouteParams = Required<MainRouteParams> & {
  tabID?: TabID;
};
