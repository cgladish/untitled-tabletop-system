/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Tabs as MaterialTabs, Tab } from "@material-ui/core";
import { useParams, useHistory, generatePath, Route } from "react-router-dom";
import * as React from "react";
import { useTheme } from "../../util/theme";
import { textFieldBorderRadius } from "../../components/TextField";
import { Vault } from "../Tabs/Vault";
import { TabID, TabRouteParams, tabRoutePath } from "./routes";
import { vaultRoutePath } from "./Vault/routes";

type Tab = {
  id: TabID;
  name: string;
};
const tabs: Tab[] = [{ id: "vault", name: "Vault" }];

export const Tabs = (): React.ReactElement | null => {
  const theme = useTheme();
  const { roomID, tabID } = useParams<TabRouteParams>();
  const history = useHistory();
  const setTab = (tab: Tab) => {
    const newParams: TabRouteParams = { roomID, tabID: tab.id };
    history.push({
      pathname: generatePath(tabRoutePath, newParams),
    });
  };

  const tabNum = tabs.findIndex(({ id }) => id === tabID);
  if (tabNum === -1) {
    setTab(tabs[0]);
    return null;
  }

  return (
    <div
      css={{
        background: theme.colors.background.secondary,
        borderRadius: textFieldBorderRadius,
        height: "100%",
      }}
    >
      <div
        css={{
          background: theme.colors.special,
          borderRadius: `${textFieldBorderRadius} ${textFieldBorderRadius} 0 0`,
        }}
      >
        <MaterialTabs
          value={tabNum}
          onChange={(_, i) => setTab(tabs[i])}
          indicatorColor="secondary"
        >
          {tabs.map(({ id, name }) => (
            <Tab key={id} label={name} />
          ))}
        </MaterialTabs>
      </div>
      <div css={{ display: "flex", height: "100%" }}>
        <Route path={`${vaultRoutePath}?`}>
          {tabID === "vault" && <Vault />}
        </Route>
      </div>
    </div>
  );
};
