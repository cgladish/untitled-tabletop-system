import * as React from "react";
import { generatePath, Route, useHistory, useParams } from "react-router-dom";
import { Menu, MenuItem } from "../../../components/Menu";
import { miscRoutePath } from "./Misc/routes";
import { Misc } from "./Misc";
import { VaultItemID, VaultRouteParams, vaultRoutePath } from "./routes";

const vaultItems: MenuItem<VaultItemID>[] = [{ id: "misc", label: "Misc" }];

export const Vault = (): React.ReactElement => {
  const { roomID, tabID, vaultItemID } = useParams<VaultRouteParams>();
  const history = useHistory();

  const setVaultItem = (vaultItem: MenuItem<VaultItemID>) => {
    const newParams: VaultRouteParams = {
      roomID,
      tabID,
      vaultItemID: vaultItem.id,
    };
    history.push({
      pathname: generatePath(vaultRoutePath, newParams),
    });
  };

  const vaultItem = vaultItems.find(({ id }) => id === vaultItemID);

  return (
    <>
      <Menu<VaultItemID>
        width={150}
        items={vaultItems}
        value={vaultItem}
        onChange={setVaultItem}
      />
      <Route path={`${miscRoutePath}?`}>
        <Misc />
      </Route>
    </>
  );
};
