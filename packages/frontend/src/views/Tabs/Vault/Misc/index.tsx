import * as React from "react";
import { generatePath, useHistory, useParams } from "react-router-dom";
import { Menu, MenuItem } from "../../../../components/Menu";
import { MiscRouteParams, miscRoutePath } from "./routes";

const mockItems: MenuItem<string>[] = [
  {
    id: "test",
    label: "Test",
  },
];

export const Misc = (): React.ReactElement => {
  const { roomID, tabID, vaultItemID, miscItemID } = useParams<
    MiscRouteParams
  >();
  const history = useHistory();

  const setMiscItem = (miscItem: MenuItem<string>) => {
    const newParams: MiscRouteParams = {
      roomID,
      tabID,
      vaultItemID,
      miscItemID: miscItem.id,
    };
    history.push({
      pathname: generatePath(miscRoutePath, newParams),
    });
  };

  const miscItem = mockItems.find(({ id }) => id === miscItemID);

  return (
    <Menu<string>
      width={150}
      items={mockItems}
      value={miscItem}
      onChange={setMiscItem}
    />
  );
};
