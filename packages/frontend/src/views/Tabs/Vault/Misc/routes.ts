import { vaultRoutePath, VaultRouteParams } from "../routes";

export const miscRoutePath = `${vaultRoutePath}/:miscItemID`;
export type MiscRouteParams = Required<VaultRouteParams> & {
  miscItemID?: string;
};
