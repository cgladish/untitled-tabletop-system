import { tabRoutePath, TabRouteParams } from "../routes";

export type VaultItemID = "misc";
export const vaultRoutePath = `${tabRoutePath}/:vaultItemID`;
export type VaultRouteParams = Required<TabRouteParams> & {
  vaultItemID?: VaultItemID;
};
