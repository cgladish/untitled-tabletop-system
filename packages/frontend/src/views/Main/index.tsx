/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Grid } from "@material-ui/core";
import { Route } from "react-router-dom";
import * as React from "react";
import { Chat } from "../../components/Chat";
import { ChatContextProvider } from "../../components/ChatContextProvider";
import { Tabs } from "../Tabs";
import { useTheme } from "../../util/theme";
import { tabRoutePath } from "../Tabs/routes";

export const Main = (): React.ReactElement | null => {
  const theme = useTheme();
  return (
    <ChatContextProvider>
      <div
        css={{
          color: theme.colors.text.primary,
          background: theme.colors.background.primary,
          padding: "10px",
          fontSize: "18px",
          height: "100%",
        }}
      >
        <Grid
          container
          css={{ height: "100%", overflow: "hidden" }}
          direction="row"
          wrap="nowrap"
        >
          <Grid
            item
            css={{
              height: "100%",
              minWidth: "20em",
              maxWidth: "20em",
              marginRight: "1em",
            }}
          >
            <Chat />
          </Grid>
          <Grid item xs={12}>
            <Route path={`${tabRoutePath}?`}>
              <Tabs />
            </Route>
          </Grid>
        </Grid>
      </div>
    </ChatContextProvider>
  );
};
