export const mainRoutePath = "/:roomID";
export type MainRouteParams = {
  roomID: string;
};
