/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { ThemeProvider } from "emotion-theming";
import { StylesProvider } from "@material-ui/core";
import { Main } from "./views/Main";
import { themes, Theme } from "./util/theme";
import { MenuBar } from "./components/MenuBar";
import { mainRoutePath } from "./views/Main/routes";

export const App = (): React.ReactElement => {
  const [theme, setTheme] = React.useState<Theme>(themes.dark);
  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <div css={{ display: "flex", flexDirection: "column", height: "100%" }}>
          <MenuBar setTheme={setTheme} />
          <Router>
            <Route exact path="/">
              Nothing here - go to /:roomID
            </Route>
            <Route path={mainRoutePath}>
              <Main />
            </Route>
          </Router>
        </div>
      </ThemeProvider>
    </StylesProvider>
  );
};
