export const REACT_APP_WEBSOCKET_URL = process.env
  .REACT_APP_WEBSOCKET_URL as string;

const requiredEnvVars = { REACT_APP_WEBSOCKET_URL };
const missingEnvVars = Object.entries(requiredEnvVars).filter(
  ([_, val]) => !val
);
if (missingEnvVars.length) {
  throw new Error(
    `Missing the following required env vars: ${missingEnvVars
      .map(([key, _]) => key)
      .join(", ")}`
  );
}
