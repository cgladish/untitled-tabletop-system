/** @jsx jsx */
import { ObjectInterpolation, jsx } from "@emotion/core";
import * as React from "react";
import merge from "lodash/merge";
import {
  TextField as MaterialTextField,
  TextFieldProps as MaterialTextFieldProps,
} from "@material-ui/core";
import { useTheme } from "../../util/theme";

export type TextFieldProps = MaterialTextFieldProps & {
  additionalCss?: ObjectInterpolation<undefined>;
};

export const textFieldBorderRadius = "8px";

export const TextField = ({
  additionalCss,
  ...restProps
}: TextFieldProps): React.ReactElement => {
  const theme = useTheme();
  return (
    <MaterialTextField
      css={merge(
        {
          input: {
            color: theme.colors.text.primary,
          },
          fieldset: {
            borderColor: theme.colors.background.tertiary,
            borderWidth: "2px",
          },
          ".MuiInputBase-root": {
            ":hover": restProps.disabled
              ? undefined
              : {
                  fieldset: {
                    borderColor: theme.colors.special,
                    borderWidth: "2px",
                  },
                },
          },
          borderRadius: textFieldBorderRadius,
        },
        additionalCss
      )}
      {...restProps}
    />
  );
};
