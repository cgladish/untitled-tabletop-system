/** @jsx jsx */
import { jsx } from "@emotion/core";
import * as React from "react";
import { Transition } from "react-transition-group";

type SlideInProps = {
  width: number;
  children: React.ReactChild | React.ReactChildren;
};

const exitedStyle = {
  minWidth: 0,
  maxWidth: 0,
};

export const SlideIn = ({
  width,
  children,
}: SlideInProps): React.ReactElement => {
  const enteredStyle = {
    minWidth: width,
    maxWidth: width,
  };
  const transitionStyles = {
    entering: enteredStyle,
    entered: enteredStyle,
    exiting: exitedStyle,
    exited: exitedStyle,
    unmounted: exitedStyle,
  };
  return (
    <Transition timeout={5000} exit={false} in appear>
      {(transitionState) => (
        <div
          css={{
            ...(transitionState in transitionStyles
              ? transitionStyles[transitionState]
              : {}),
            transition: `min-width, min-width 0.5s ease-in-out, max-width 0.5s ease-in-out`,
          }}
        >
          {children}
        </div>
      )}
    </Transition>
  );
};
