/** @jsx jsx */
import { jsx } from "@emotion/core";
import * as React from "react";
import { useTheme } from "../../util/theme";
import { SlideIn } from "../SlideIn";

export type MenuItem<TItemID extends string> = {
  id: TItemID;
  label: string;
};

type MenuProps<TItemID extends string> = {
  width: number;
  items: MenuItem<TItemID>[];
  value: MenuItem<TItemID> | undefined;
  onChange: (item: MenuItem<TItemID>) => void;
};

export function Menu<TItemID extends string>({
  width,
  items,
  value,
  onChange,
}: MenuProps<TItemID>): React.ReactElement {
  const theme = useTheme();
  return (
    <SlideIn width={width}>
      <ul
        css={{
          display: "flex",
          flexDirection: "column",
          borderRight: `2px solid ${theme.colors.background.primary}`,
          height: "100%",
          li: {
            width: "100%",
            height: "2em",
            display: "flex",
            alignItems: "center",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipses",
            cursor: "pointer",
            borderLeft: `0 solid ${theme.colors.background.secondary}`,
            transition: "border-color .25s, border-left-width .25s",
            padding: "0 .5em",
            "&:hover, &.active": {
              borderColor: theme.colors.special,
              borderLeftWidth: ".3em",
            },
          },
        }}
      >
        {items.map((item) => {
          const isActive = value?.id === item.id;
          return (
            <li
              key={item.id}
              className={isActive ? "active" : undefined}
              onClick={() => onChange(item)}
            >
              {item.label}
            </li>
          );
        })}
      </ul>
    </SlideIn>
  );
}
