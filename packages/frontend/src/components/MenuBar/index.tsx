/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Switch } from "@material-ui/core";
import { Brightness2, Brightness7 } from "@material-ui/icons";
import * as React from "react";
import { Theme, useTheme, themes } from "../../util/theme";

type MenuBarProps = {
  setTheme: (theme: Theme) => void;
};

export const MenuBar = ({ setTheme }: MenuBarProps): React.ReactElement => {
  const theme = useTheme();
  return (
    <div
      css={{
        width: "100%",
        background: theme.colors.special,
        height: "50px",
        display: "flex",
        alignItems: "center",
        padding: "0 10px",
      }}
    >
      <div css={{ display: "flex", alignItems: "center" }}>
        <Brightness2 css={{ color: "#222" }} />
        <Switch
          value={theme.id === "dark" ? "off" : "on"}
          onChange={(_, checked) =>
            setTheme(checked ? themes.light : themes.dark)
          }
          css={{
            ".MuiSwitch-switchBase": {
              color: "#222",
              "+ .MuiSwitch-track": { background: "#222", opacity: ".8" },
            },
            ".MuiSwitch-colorSecondary.Mui-checked": {
              color: "#fff",
              "+ .MuiSwitch-track": { background: "#fff", opacity: ".8" },
            },
          }}
        />
        <Brightness7 css={{ color: "#fff" }} />
      </div>
    </div>
  );
};
