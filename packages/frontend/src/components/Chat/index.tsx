/** @jsx jsx */
import { jsx } from "@emotion/core";
import { TextField, textFieldBorderRadius } from "../TextField";
import * as React from "react";
import { useTheme } from "../../util/theme";
import { CircularProgress } from "@material-ui/core";
import { Send } from "@material-ui/icons";
import { ChatContext, Message } from "../ChatContextProvider";

export const Chat = (): React.ReactElement => {
  const theme = useTheme();

  const [chatText, setChatText] = React.useState<string>("");
  const { isConnected, addMessageHandler, sendText } = React.useContext(
    ChatContext
  );

  const [messages, setMessages] = React.useState<Message[]>([]);
  // This is janky but if we directly use setMessages in the message handler then
  // we end up just replacing the messages list every time instead
  const [latestMessage, setLatestMessage] = React.useState<Message | null>(
    null
  );

  React.useEffect(() => {
    const removeMessageHandler = addMessageHandler((message) => {
      setLatestMessage(message);
    });
    return () => removeMessageHandler();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    if (latestMessage) {
      setMessages([...messages, latestMessage]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [latestMessage]);

  const submitChat = () => {
    setChatText("");
    sendText(chatText);
  };

  return (
    <div
      css={{
        background: theme.colors.background.secondary,
        borderRadius: textFieldBorderRadius,
        height: "100%",
        width: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <div css={{ height: "100%" }}>
        {!isConnected && (
          <div
            css={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              height: "100%",
              color: theme.colors.special,
            }}
          >
            <div css={{ fontSize: "1.25em", marginBottom: ".5em" }}>
              Connecting...
            </div>
            <CircularProgress color="inherit" />
          </div>
        )}
        {messages.map((message, i) => (
          <div key={i} css={{ margin: "1em" }}>
            {message.content}
          </div>
        ))}
      </div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          submitChat();
        }}
      >
        <TextField
          variant="outlined"
          additionalCss={{
            fieldset: {
              borderRadius: `0 0 ${textFieldBorderRadius} ${textFieldBorderRadius}`,
            },
            width: "100%",
          }}
          placeholder="Type text here..."
          InputProps={{
            endAdornment: (
              <Send
                css={{
                  cursor: isConnected ? "pointer" : undefined,
                  color: isConnected
                    ? theme.colors.special
                    : theme.colors.text.tertiary,
                }}
                onClick={submitChat}
              />
            ),
          }}
          value={chatText}
          onChange={(e) => setChatText(e.target.value)}
          disabled={!isConnected}
        />
      </form>
    </div>
  );
};
