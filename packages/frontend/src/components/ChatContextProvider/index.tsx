import * as React from "react";
import { useParams } from "react-router-dom";
import { REACT_APP_WEBSOCKET_URL } from "../../constants";
import { MainRouteParams } from "../../views/Main";

export type Message = {
  roomID: string;
  date: Date;
  type: "text";
  content: any;
};

type MessageHandler = (message: Message) => void;

type ChatContext = {
  isConnected?: boolean;
  addMessageHandler: (messageHandler: MessageHandler) => () => void;
  sendText: (messageText: string) => void;
};
export const ChatContext = React.createContext<ChatContext>({
  isConnected: false,
  addMessageHandler: () => () => {},
  sendText: () => {},
});

const RETRY_CHAT_CONNECTION_TIMER = 5000;

export const ChatContextProvider = (props: {
  children: React.ReactChild | React.ReactChildren;
}): React.ReactElement => {
  const [webSocket, setWebSocket] = React.useState<WebSocket | null>(null);
  const [isConnected, setIsConnected] = React.useState<boolean>(false);
  const [messageHandlers, setMessageHandlers] = React.useState<
    MessageHandler[]
  >([]);
  const { roomID } = useParams<MainRouteParams>();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const createNewWebSocket = () => {
    try {
      const newWebSocket = new WebSocket(REACT_APP_WEBSOCKET_URL);
      newWebSocket.onclose = () => createNewWebSocket();
      newWebSocket.onopen = () => setIsConnected(true);
      newWebSocket.onerror = () => setIsConnected(false);
      setWebSocket(newWebSocket);
    } catch (err) {
      console.error("Unable to create chat connection");
      console.error(err);
      setWebSocket(null);
      setTimeout(() => createNewWebSocket(), RETRY_CHAT_CONNECTION_TIMER);
    }
  };

  React.useEffect(() => {
    try {
      if (webSocket) {
        webSocket.close();
      } else {
        createNewWebSocket();
      }
    } catch (err) {
      console.error("Unable to close chat connection");
      console.error(err);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [roomID]);

  React.useEffect(() => {
    if (webSocket) {
      webSocket.onmessage = (event) => {
        let message: Message;
        try {
          message = JSON.parse(event.data);
          message.date = new Date(message.date);
        } catch (err) {
          console.error("Unable to parse message data");
          console.error(err);
          return;
        }
        messageHandlers.forEach((messageHandler) => messageHandler(message));
      };
    }
  }, [webSocket, messageHandlers]);

  return (
    <ChatContext.Provider
      value={{
        isConnected,
        addMessageHandler: (messageHandler) => {
          setMessageHandlers([...messageHandlers, messageHandler]);
          const removeMessageHandler = () => {
            const newMessageHandlers = messageHandlers.filter(
              (handler) => handler !== messageHandler
            );
            setMessageHandlers(newMessageHandlers);
          };
          return removeMessageHandler;
        },
        sendText: (text) => {
          if (!webSocket) {
            return;
          }
          const message: Message = {
            roomID,
            date: new Date(),
            type: "text",
            content: text,
          };
          try {
            webSocket.send(JSON.stringify(message));
          } catch (err) {
            console.error("Unable to send message");
            console.error(err);
          }
        },
      }}
    >
      {props.children}
    </ChatContext.Provider>
  );
};
