import express from "express";
import { createServer } from "http";
import { API_PORT } from "./constants";

const app = express();
const server = createServer(app);

server.listen(API_PORT, () => {
  console.log(`Api listening at http://localhost:${API_PORT}`);
});
