if (process.env.NODE_ENV === "development") {
  require("dotenv").config({ path: "../../.env" });
}

export const API_PORT = process.env.API_PORT as string;

const requiredEnvVars = { API_PORT };
const missingEnvVars = Object.entries(requiredEnvVars).filter(
  ([_, val]) => !val
);
if (missingEnvVars.length) {
  throw new Error(
    `Missing the following required env vars: ${missingEnvVars
      .map(([key, _]) => key)
      .join(", ")}`
  );
}
