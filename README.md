# untitled-tabletop-system

Repository for an implementation of a helper web app for an as-yet untitled tabletop RPG system.

### Getting started

Run `yarn` to install all required depencies.

Copy `.env.sample` to `.env` and fill in any missing variables.

To start the web server, run

```
yarn start:dev
```

To start the API & websocket server, run

```
yarn api:dev
```
